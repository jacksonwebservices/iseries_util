from unittest import mock

import pyodbc
import pytest

from iseries_util import cl_factory
from iseries_util.cli.copy_objects import Copier, regex_extract_groups


@pytest.fixture()
def mock_cursor(monkeypatch):
    mock_cursor = mock.MagicMock(spec=pyodbc.Cursor)
    monkeypatch.setattr(Copier, 'cursor', mock_cursor)
    yield mock_cursor


@pytest.fixture()
def copier():
    copier = Copier('', '', schemas=[], objects=[], whatif=False)
    return copier


def test_copier_exec__cl(copier, mock_cursor):
    cl = cl_factory('TESTCMD')(arg1=1, arg2=2)
    copier.exec(cl)
    assert mock_cursor.execute.call_count == 1
    assert mock_cursor.execute.call_args == mock.call('call qsys2.qcmdexc(?)', ['TESTCMD arg1(1) arg2(2)'])


@pytest.mark.parametrize('txt,expected', [
    ('A=1;B=2;C=3', ('1', '2', '3')),
    ('C=3;A=1;B=2', ('1', '2', '3')),
    ('B=2;C=3;A=1', ('1', '2', '3')),
    ('C=3;A=1', ('1', None, '3')),
    ('', (None, None, None))
])
def test_regex_extract_groups(txt, expected):
    actual = regex_extract_groups([
        'A=(1)',
        'B=(2)',
        'C=(3)',
    ], txt)
    assert actual == expected
