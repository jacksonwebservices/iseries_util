import logging
from typing import Any, NamedTuple, Sequence, Callable, Tuple, List

import pyodbc
import re
from functools import lru_cache, partial
from itertools import zip_longest
from pathlib import Path

log = logging.getLogger('iseries_util')


EXAMPLE_DSN = """[ODBC]
DRIVER=iSeries Access ODBC Driver
SYSTEM={system}
UID={username}
PWD={password}
GRAPHIC=1
SIGNON=1
QRYSTGLMT=-1
PKG=QGPL/DEFAULT(IBM),2,0,1,0,512
LANGUAGEID=ENU
DFTPKGLIB=QGPL
NAM=1
CMT=2
DB2SQLSTATES=1
UNICODESQL=1"""


class CLArg(NamedTuple):
    arg: str
    val: Any

    def __str__(self):
        return f'{self.arg}({self.val})'


class CLCmd(NamedTuple):
    cmd: str
    args: Sequence[CLArg]

    @classmethod
    def build(cls, cmd, **kwargs):
        args = [CLArg(a, v) for (a, v) in kwargs.items()]
        return cls(cmd=cmd, args=args)

    def __str__(self):
        args = ' '.join(f'{a}' for a in self.args)
        return f'{self.cmd} {args}'

    def sql_and_params(self) -> Tuple[str, List[str]]:
        return f"call qsys2.qcmdexc(?)", [str(self)]


def cl_factory(cl_cmd) -> Callable[..., CLCmd]:
    """Examples: RSTOBJ = cl_factory('RSTOBJ'), DLTF = CLCmd('DLTF')"""
    return partial(CLCmd.build, cl_cmd)


CLRSAVF = cl_factory('CLRSAVF')
CRTSAVF = cl_factory('CRTSAVF')
CRTSRCPF = cl_factory('CRTSRCPF')
DLTF = cl_factory('DLTF')
DLTOVR = cl_factory('DLTOVR')
DSPJOBLOG = cl_factory('DSPJOBLOG')
FTP = cl_factory('FTP')
OVRDBF = cl_factory('OVRDBF')
RSTLIB = cl_factory('RSTLIB')
RSTOBJ = cl_factory('RSTOBJ')
RUNSQL = cl_factory('RUNSQL')
SAVLIB = cl_factory('SAVLIB')
SAVOBJ = cl_factory('SAVOBJ')


# noinspection PyPep8Naming
def CL(cmd, **kwargs):
    """
    Generates SQL to call a CL command
    https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_73/rzajq/rzajqprocqcmdexc.htm
    """
    cmd = CLCmd(cmd, [CLArg(a, v) for (a, v) in kwargs.items()])
    cmd_str = str(cmd).replace("'", "''")  # escape for sql

    return f"call qsys2.qcmdexc('{cmd_str}')"


def copy_schema(connection, source, dest):
    c = connection.cursor()
    c.execute(f'CREATE SCHEMA {dest}')
    c.commit()
    c.execute(f"SELECT SYSTEM_TABLE_SCHEMA FROM QSYS2.SYSTABLES WHERE TABLE_SCHEMA = '{source}' LIMIT 1")
    source_short_name = c.fetchval().strip()
    c.execute(f"SELECT SYSTEM_TABLE_SCHEMA FROM QSYS2.SYSTABLES WHERE TABLE_SCHEMA = '{dest}' LIMIT 1")
    dest_short_name = c.fetchval().strip()
    try:
        c.execute(CL(f"CPYLIB FROMLIB( {source_short_name} ) TOLIB( {dest_short_name} ) CRTLIB( *NO ) DATA( *NO )"))
    except pyodbc.Error as e:
        if 'CPF2358' in str(e):  # some libraries may not copy or may copy partially (error message is vague)
            log.warning(str(e))
        else:
            raise
    c.close()


def drop_schema(connection, schema_name):
    prev_autocommit_state = connection.autocommit
    connection.autocommit = True  # DROP SCHEMA command does not work unless autocommit is True
    c = connection.cursor()
    c.execute(f"SELECT SYSTEM_TABLE_SCHEMA FROM QSYS2.SYSTABLES WHERE TABLE_SCHEMA = '{schema_name}' LIMIT 1")
    short_schema_name = c.fetchval().strip()

    stop_journalling_cmds = [
        CL(f'ENDJRNLIB LIB( {short_schema_name} )'),
        CL(f'ENDJRNPF FILE( *ALL ) JRN( {short_schema_name}/QSQJRN )'),
        CL(f'DLTJRN JRN( {short_schema_name}/QSQJRN )'),
        CL(f'DLTJRNRCV JRNRCV( {short_schema_name}/QSQJRN* ) DLTOPT( *IGNINQMSG )'),
    ]
    for cmd in stop_journalling_cmds:
        try:
            c.execute(cmd)
            log.debug(f'success in {cmd}')
        except pyodbc.Error as e:
            log.error(f'error in {cmd}')

    try:
        c.execute(f'DROP SCHEMA {schema_name}')
    except pyodbc.Error as e:
        log.error(f'Could not tear down temporary schema: {schema_name}')
        raise
    finally:
        c.close()
        connection.autocommit = prev_autocommit_state


def fetch_dicts(cursor):
    """
    Run this after a SELECT or CALL that has returned a record set. Returns a list of {column name: val} dicts

    Column names will be converted to lowercase
    """

    def row_dict(row):
        return {col[0].lower(): val for col, val in zip(cols, row)}

    cols = cursor.description
    rows = cursor.fetchall()

    return [row_dict(row) for row in rows]


def fetch_dict(cursor):
    """
    Run this after a SELECT or CALL that has returned a record set. Returns a dict of {column name: val}

    Column names will be converted to lowercase
    """

    def row_dict(row):
        return {col[0].lower(): val for col, val in zip(cols, row)}

    cols = cursor.description
    row = cursor.fetchone()

    return row_dict(row)


def call_procedure(cursor, procedure_name, params):
    """
    Calls a db2 procedure in the current path with the given params. Supply garbage/dummy params
    as placeholders for for output parameters.

    returns a dictionary of the output params
    """
    # TODO: refactor to work with overloaded procedures
    param_defs = get_param_defs(cursor, procedure_name)
    param_pairs = list(zip_longest(params, param_defs, fillvalue=OUT_PARAM))  # assume output params after all inputs

    index_last_input_param = next(
        len(param_defs) - i - 1
        for i, p in enumerate(reversed(param_defs)) if p['parameter_mode'] in ['IN', 'INOUT']
    )
    if len(params) - 1 < index_last_input_param:
        raise ValueError(f'minimum {index_last_input_param} parameters required, {len(params)} supplied')

    # prepare the global variables and sql call string
    variables = []
    param_strings = []
    input_values = []
    for p, p_def in param_pairs:
        var_name = p_def['parameter_name']
        var_type = _type_string(p_def)
        if p_def['parameter_mode'] == 'INOUT':
            cursor.execute(f"create or replace variable {var_name} {var_type} default {p}")
            variables.append(var_name)
            param_strings.append(var_name)
        elif p_def['parameter_mode'] == 'OUT':
            cursor.execute(f"create or replace variable {var_name} {var_type}")
            variables.append(var_name)
            param_strings.append(var_name)
        elif p_def['parameter_mode'] == 'IN':
            param_strings.append('?')
            input_values.append(p)

    param_string = ', '.join(param_strings)

    # call the function
    cursor.execute(f'call {procedure_name} ( {param_string} )', input_values)

    # query the returned values from the global variables
    returned_variables = {}
    if variables:
        variables_string = ', '.join(variables)
        cursor.execute(f'VALUES( {variables_string} )')
        values = cursor.fetchone()

        # drop the global variables
        for v in variables:
            cursor.execute(f'drop variable {v}')

        returned_variables = dict(zip(variables, values))
    return returned_variables


@lru_cache()
def get_param_defs(cursor, procedure_name):
    select_param_defs = """
SELECT B.ORDINAL_POSITION                          AS ORD,
       B.PARAMETER_NAME                            AS PARAMETER_NAME,
       B.PARAMETER_MODE                            AS PARAMETER_MODE,
       B.DATA_TYPE                                 AS TYPE,
       B.DATA_TYPE_NAME                            AS TYPE_NAME,
       DEC(COALESCE(B.CHARACTER_MAXIMUM_LENGTH,
                    B.NUMERIC_PRECISION, 0), 5, 0) AS SIZE,
       DEC(IFNULL(B.NUMERIC_SCALE, 0), 5, 0)       AS DECS,
       A.SPECIFIC_NAME                             AS SPECIFIC_NAME,
       A.ROUTINE_SCHEMA                            AS ROUTINE_SCHEMA

FROM QSYS2.SYSPROCS A
       INNER JOIN QSYS2.SYSPARMS B ON A.SPECIFIC_NAME = B.SPECIFIC_NAME AND
                                      A.SPECIFIC_SCHEMA = B.SPECIFIC_SCHEMA

WHERE (A.ROUTINE_SCHEMA = CURRENT_SCHEMA)
  AND A.ROUTINE_NAME = UPPER(?)

ORDER BY A.ROUTINE_SCHEMA, A.SPECIFIC_SCHEMA,
         A.SPECIFIC_NAME, ORD

    FOR READ ONLY;
    """.strip().rstrip(';')
    cursor.execute(select_param_defs, procedure_name)
    param_defs = fetch_dicts(cursor)
    return param_defs


def _type_string(p):
    """For a parameter description, returns the string to describe its type in db2 SQL"""
    type_name = p['type']
    type_param = p['decs'] or p['size'] or ''
    if type_param:
        type_param = '(' + str(type_param) + ')'
    return ' '.join([type_name, type_param])


def get_sql_state(pyodbc_exception):
    try:
        return pyodbc_exception.value.args[0]
    except (AttributeError, KeyError, IndexError):
        return None


def get_current_user(c):
    c.execute('SELECT CURRENT_USER FROM SYSIBM.SYSDUMMY1')
    username = c.fetchval()
    return username


def read_file_dsn(dsn_file):
    """Reads dsn settings from file"""
    p = Path(dsn_file)
    file_dsn_contents = p.read_text().strip().split('\n')
    odbc_string = ';'.join(row for row in file_dsn_contents if not row.startswith('['))
    return odbc_string


OUT_PARAM = None  # garbage placeholder value for OUT parameters in stored procedure calls
SQL_STATE_DUPLICATE_ENTRY = '23505'


def split_sql(sql_text):
    """
    Supports statements terminated by a ';' and multiline statements wrapped in BEGIN/END (no ';')

    Does NOT support nested BEGIN/END
    """
    sql_commands = re.findall("[^;']+BEGIN\s.*?\sEND(?! IF)|\s[^;]{3}(?<!END)[^;]*;(?!\s+END)", sql_text,
                              flags=re.MULTILINE | re.DOTALL | re.IGNORECASE)
    sql_commands = [s.strip().rstrip(';') for s in sql_commands]
    return sql_commands