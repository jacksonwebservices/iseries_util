import datetime
import logging
import re
import sys
from itertools import groupby, chain
from pathlib import Path
from pprint import pformat
from typing import NamedTuple, List, Sequence, Tuple, Iterable, Mapping, Union

import click
import pyodbc

from iseries_util import EXAMPLE_DSN, CRTSRCPF, RSTLIB, RSTOBJ, SAVOBJ, CRTSAVF, RUNSQL, CLCmd, cl_factory, \
    read_file_dsn, SAVLIB, CLRSAVF, OVRDBF, FTP, DLTF, DLTOVR

logging.basicConfig(
    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s'
)
log = logging.getLogger('copy_objects')


class ObjectSpec(NamedTuple):
    library: str
    object_name: str


def regex_extract_groups(patterns, target):
    """
    Creates non-consuming lookahead groups for each given regex pattern in order to extract matched text
    from target which can appear in any given order.

    adapted from
    https://stackoverflow.com/questions/37447983/regexp-to-match-random-order-words
    """
    pattern = ''.join([f'(?=(?:.*{p})?)' for p in patterns])
    return re.search(pattern, target).groups()


class Copier:
    def __init__(self, src_connection_string, dst_connection_string, schemas, objects, whatif, save_file_lib='QTEMP'):
        """
        This utility class will copy the structure and data libraries and objects from <src> iSeries to <dst> iSeries.

        .copy():
            after this has been run, the destination database will have complete copies of the specified
            schemas and objects from the source database

        :param src_connection_string: odbc connection string for source iSeries box
        :param dst_connection_string: odbc connection string for destination iSeries box
        :param schemas: schema names (long)
        :param objects: object names (dotted long)
        :param save_file_lib: schema name for temporary save file (long)
        """
        self.src_connection_string = src_connection_string
        self.dst_connection_string = dst_connection_string

        schemas = [schemas] if isinstance(schemas, str) else schemas
        objects = [objects] if isinstance(objects, str) else objects
        self.schemas = [s.upper() for s in schemas]
        self.objects = [o.upper() for o in objects]

        self.save_file_lib = save_file_lib
        self.whatif = whatif

        self._src = None
        self._cursor = None
        self._obj_spec_cache = None
        self._job_name = None

    def copy(self):
        if not (self.schemas or self.objects):
            log.info('nothing specified to copy')
            return

        schema_saves, object_saves = self.create_src_save_files()
        self.copy_save_file_to_dst(schema_saves, object_saves)
        self.delete_src_save_files(schema_saves, *object_saves.values())

    def create_src_save_files(self) -> Tuple[str, Mapping]:
        """Copy schemas and objects to temp seed library and save it to a save file"""
        # create a save file
        schema_save_file = 'QTEMP/TMPCPYLIB'
        self._create_save_file(schema_save_file)

        # copy schemas to save file
        schema_short_names = self.get_schema_short_names()
        if schema_short_names:
            log.info(f'saving {schema_short_names} to temp save file')
            src_lib_specifier = ' '.join(schema_short_names)
            self.exec(SAVLIB(lib=src_lib_specifier, dev='*SAVF', savf=schema_save_file))

        # copy objects to save file
        object_save_files = {}
        for idx, (library, object_specs) in enumerate(self.get_object_specs_by_library()):
            obj_save_file = f'QTEMP/TMPCPY{idx:04}'
            self._create_save_file(obj_save_file)
            object_names = [os.object_name for os in object_specs]

            log.info(f'saving {object_names} from {library} to temp save file')
            self.exec(SAVOBJ(
                obj=' '.join(object_names),
                lib=library,
                dev='*SAVF',
                savf=obj_save_file,
                savact='*LIB',
                # 10 seconds to obtain lock, don't wait for commit boundary (could result in partial transactions)
                savactwait='10 *NOCMTBDY 10'
            ))
            object_save_files[library] = obj_save_file

        return schema_save_file, object_save_files

    def copy_save_file_to_dst(self, schema_saves: str, object_saves: Mapping):
        """Copy file from src to dst in seed library on each"""
        dst_host, dst_user, dst_pass = regex_extract_groups([
            'SYSTEM=([^;]+)',
            'UID=([^;]+)',
            'PWD=([^;]+)',
        ], self.dst_connection_string)

        tmp_files = [schema_saves, *object_saves.values()]
        create_cmds = [f'QUOTE RCMD {CRTSAVF(file=f)}' for f in tmp_files]
        put_cmds = [f'PUT {f} {f}' for f in tmp_files]

        restore_schema_cmds = self._restore_schemas_cmd(schema_saves)
        restore_obj_cmds = self._restore_objs_cmds(object_saves)
        restore_cmds = [f'QUOTE RCMD {cmd}' for cmd in chain(restore_schema_cmds, restore_obj_cmds)]

        save_journal_sql = re.sub('[ \n]+', ' ', '''
        create table qtemp.tmplog as
            (
                select message_id, message_type, severity, message_timestamp, message_text, message_second_level_text
                    from table (qsys2.joblog_info(job_name))
            ) with data''').strip()

        ftp_cmds = [f"{dst_user} {dst_pass}",
                    f"BINARY",
                    *create_cmds,
                    *put_cmds,
                    *restore_cmds,
                    'QUOTE RCMD {}'.format(RUNSQL(sql=f"'{save_journal_sql}'", commit='*NC')),
                    'QUOTE RCMD {}'.format(CRTSAVF(file='QTEMP/XFERJOBLOG')),
                    'QUOTE RCMD {}'.format(
                        SAVOBJ(obj='TMPLOG', lib='QTEMP', dev='*SAVF', savf='QTEMP/XFERJOBLOG', savact='*LIB')
                    ),
                    'GET QTEMP/XFERJOBLOG QTEMP/XFERJOBLOG (REPLACE',  # the unmatched parenthesis is correct
                    f"CLOSE",
                    f"QUIT"]

        log.info('creating temporary src file with ftp commands')
        ftp_file = 'QTEMP/FTPTMP'
        ftp_cmds_mbr = 'FTPCMDS'
        ftp_log_mbr = 'FTPLOG'

        record_length = max(120, *[len(c) for c in ftp_cmds]) + 12
        self.exec(CRTSRCPF(file=ftp_file, rcdlen=record_length, mbr=ftp_cmds_mbr))
        self.exec(f'CREATE ALIAS QTEMP.{ftp_cmds_mbr} for QTEMP.FTPTMP ( {ftp_cmds_mbr} )')
        try:
            self.exec(CRTSAVF(file='QTEMP/XFERJOBLOG'))
        except pyodbc.Error:
            self.exec(CLRSAVF(file='QTEMP/XFERJOBLOG'))

        today = datetime.date.today()
        cmd = f'INSERT INTO QTEMP.{ftp_cmds_mbr} (SRCSEQ, SRCDAT, SRCDTA) VALUES (?, ?, ?) WITH NC'
        src_file_lines = [(idx, today.strftime('%y%m%d'), line) for (idx, line) in enumerate(ftp_cmds)]
        log.debug(pformat(src_file_lines[1:], width=200))
        self.exec_many(cmd, src_file_lines)

        # redirect input/output for ftp pgm
        self.exec(OVRDBF(file='INPUT', tofile=ftp_file, mbr=ftp_cmds_mbr, ovrscope='*JOB'))
        self.exec(OVRDBF(file='OUTPUT', tofile=ftp_file, mbr=ftp_log_mbr, ovrscope='*JOB'))

        log.info('copying to destination and performing restore (RST***) commands via FTP')
        self.exec(FTP(rmtsys='*INTNETADR', intnetadr=f"'{dst_host}'"))

        # get ftp log
        sql = f'CREATE ALIAS QTEMP.{ftp_log_mbr} FOR QTEMP.FTPTMP ({ftp_log_mbr})'
        self.exec(sql)

        sql = 'select trim(srcdta) from qtemp.ftplog'
        try:
            c = self.exec(sql)
        except pyodbc.ProgrammingError as e:
            if e.args[0] == '42704':
                log.warning('No ftp logs detected!')
        else:
            log_lines = c.fetchall()
            log.debug(pformat([line for (line, ) in log_lines], width=150))

        # get remote job log (for errors)
        self.exec(RSTOBJ(obj='TMPLOG', savlib='QTEMP', dev='*SAVF', savf='QTEMP/XFERJOBLOG'))
        if not self.whatif:
            c = self.exec('''select * from qtemp.tmplog where severity >= 20''')
            log_lines = c.fetchall()
            for line in log_lines:
                if line.SEVERITY > 30:
                    log.error(pformat(line, width=200))
                else:
                    log.debug(pformat(line, width=200))

        log.info('cleaning up temporary files')

        # remove redirects
        self.exec(DLTOVR(file='INPUT OUTPUT', lvl='*JOB'))

        # remove tmp src file with password
        self.exec(DLTF(file=ftp_file))

    def _create_save_file(self, schema_save_file):
        try:
            self.exec(CRTSAVF(file=schema_save_file))
        except pyodbc.InterfaceError as e:
            log.exception(e)
            raise
        except pyodbc.Error:
            log.warning(f'Temp copy save file already exists. Using existing save file.')
            self.exec(CLRSAVF(file=schema_save_file))

    def get_schema_short_names(self):
        src_short_names = []
        for s in self.schemas:
            c = self.exec((f"SELECT SYSTEM_SCHEMA_NAME FROM QSYS2.SYSSCHEMAS"
                           f" WHERE SCHEMA_NAME = ?"), s)
            src_short_names.append(c.fetchone()[0].strip())

        return src_short_names

    def get_object_specs_by_library(self) -> Iterable[Tuple[str, List[ObjectSpec]]]:
        def get_library(s):
            return s.library

        object_specs = self._make_object_specs(self.objects)
        object_specs.sort(key=get_library)
        objects_by_library = groupby(object_specs, key=get_library)
        return objects_by_library

    def delete_src_save_files(self, *args):
        for save_file in args:
            self.exec(DLTF(file=save_file))

    @property
    def src(self) -> pyodbc.Connection:
        if self._src is None:
            try:
                self._src = pyodbc.connect(self.src_connection_string)
            except pyodbc.InterfaceError as e:
                if e.args[0] == '28000':
                    raise Exception('Authentication failure: check user credentials')
                raise
        return self._src

    @property
    def cursor(self) -> pyodbc.Cursor:
        cursor = self._cursor
        if cursor is None:
            self._cursor = cursor = self.src.cursor()
            cursor.execute('SELECT JOB_NAME FROM SYSIBM.sysdummy1')
            self._job_name = job_name = cursor.fetchone()[0]
            log.debug(f'cursor opened with job name {job_name}')
        return cursor

    def _make_object_specs(self, objects) -> List[ObjectSpec]:
        results = self._obj_spec_cache
        if results:
            return results

        self._obj_spec_cache = results = []
        for obj in objects:
            library, table_name = obj.split('.')
            cmd = (f"SELECT SYSTEM_TABLE_SCHEMA, SYSTEM_TABLE_NAME FROM QSYS2.SYSTABLES "
                   f" WHERE (TABLE_SCHEMA, TABLE_NAME) IN (VALUES (?, ?))")
            c = self.exec(cmd, (library, table_name))
            found = c.fetchone()
            if not found:
                cmd = (f"SELECT SYSTEM_TABLE_SCHEMA, SYSTEM_TABLE_NAME FROM QSYS2.SYSTABLES "
                       f" WHERE (TABLE_SCHEMA, SYSTEM_TABLE_NAME) IN (VALUES (?, LEFT(CAST(? as varchar(256)), 11)))")
                c = self.exec(cmd, (library, table_name))
                found = c.fetchone()
                if not found:
                    raise Exception('Objects not found', library, table_name)
            results.append(ObjectSpec(*(val.strip() for val in found)))
        return results

    def _restore_schemas_cmd(self, tmp_save_file):
        schemas = ' '.join(self.get_schema_short_names())
        if not schemas:
            return []

        rstlib = schemas

        return [RSTLIB(savlib=schemas, dev='*SAVF', savf=tmp_save_file, alwobjdif='*COMPATIBLE', mbropt='*ALL',
                       rstlib=rstlib)]

    def _restore_objs_cmds(self, object_saves):
        cmds = []
        for library, object_specs in self.get_object_specs_by_library():
            str_objs = ' '.join(obj.object_name for obj in object_specs)
            cl_cmd = RSTOBJ(
                obj=str_objs, savlib=library, dev='*SAVF', savf=object_saves[library],
                alwobjdif='*COMPATIBLE', mbropt='*ALL'
            )
            cmds.append(cl_cmd)
        return cmds

    def exec(self, sql_or_cl: Union[str, CLCmd], params=None) -> pyodbc.Cursor:
        """execute raw SQL or a CLCmd instance (will be translated to SQL)"""
        if isinstance(sql_or_cl, CLCmd):
            sql, params = sql_or_cl.sql_and_params()
        else:
            sql = sql_or_cl

        log.debug(sql)
        if not sql.lower().startswith('select') and self.whatif:
            return self.cursor
        if params is None:  # pyodbc does not seem to support execute(sql, None) syntax
            return self.cursor.execute(sql)
        return self.cursor.execute(sql, params)

    def exec_many(self, sql, params: Sequence[Sequence] = None) -> pyodbc.Cursor:
        log.debug('batch:' + sql)
        if self.whatif:
            return self.cursor
        return self.cursor.executemany(sql, params)

    def exec_cl(self, cl_cmd: Union[str, CLCmd], **kwargs) -> pyodbc.Cursor:
        """example: self.exec_cl('TESTCMD', arg1=1)"""
        if isinstance(cl_cmd, str):
            cl_cmd = cl_factory(cl_cmd)(**kwargs)
        sql, params = cl_cmd.sql_and_params()
        return self.exec(sql, params=params)


@click.group(invoke_without_command=True)
@click.pass_context
@click.option('--source-dsn', help="Specify source file DSN", default='source.dsn', show_default=True)
@click.option('--destination-dsn', help="Specify destination file DSN", default='dest.dsn', show_default=True)
@click.option('--schema', required=False, help="Specify source schema/library", multiple=True)
@click.option('--obj', required=False, help="Specify source object (fmt: SCHEMA.OBJ)", multiple=True)
@click.option('--whatif', is_flag=True, help="Specify this option to see what actions would occur "
                                             "(no changes will be made)")
@click.option('-v', '--verbose', count=True, help="Increase diagnostic output (max -vvv)")
def copy_objects(ctx, source_dsn, destination_dsn, schema, obj, verbose, whatif):
    """
    Copies objects/libraries from a source system to a destination system
    """
    log_level = [
        logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG
    ][max(verbose, 3)]
    log.setLevel(log_level)
    logging.basicConfig(level=log_level)
    if ctx.invoked_subcommand is None:
        src_dsn = read_file_dsn(source_dsn)
        dst_dsn = read_file_dsn(destination_dsn)

        s = Copier(src_dsn, dst_dsn, schema, obj, whatif)
        s.copy()


@copy_objects.command()
@click.option('--source-system', prompt="Source system IP or DNS")
@click.option('--source-username', prompt="Source system username")
@click.option('--source-password', prompt="Source system password", hide_input=True)
@click.option('--dest-system', prompt="Destination system IP or DNS")
@click.option('--dest-username', prompt="Destination system username")
@click.option('--dest-password', prompt="Destination system password", hide_input=True)
def create_dsn_files(source_system, source_username, source_password, dest_system, dest_username, dest_password):
    """create ./source.dsn and ./dest.dsn from template"""
    src = Path('./source.dsn')
    dst = Path('./dest.dsn')

    log.info(f'writing to {src}')
    src.write_text(
        EXAMPLE_DSN.format(system=source_system, username=source_username, password=source_password)
    )

    log.info(f'writing to {dst}')
    dst.write_text(
        EXAMPLE_DSN.format(system=dest_system, username=dest_username, password=dest_password)
    )


if __name__ == '__main__':
    copy_objects()
