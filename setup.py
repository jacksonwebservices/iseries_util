#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('HISTORY.md') as history_file:
    history = history_file.read()

requirements = ['Click>=7.0', 'pyodbc>=4.0.30']

test_requirements = ['pytest>=3']

setup(
    author="Steven James",
    author_email='sjames@jacksonfurnind.com',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="A set of CLI and API tools for interfacing with iSeries through ODBC",
    entry_points={
        'console_scripts': [
            'copy_objects=iseries_util.cli.copy_objects:copy_objects',
        ],
    },
    install_requires=requirements,
    license="MIT License",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='iseries',
    name='iseries_util',
    packages=find_packages(include=['iseries_util', 'iseries_util.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://bitbucket.org/sjames-jackson/iseries_util',
    version='0.1',
    zip_safe=False,
)
