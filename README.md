## Installation

Prerequisites: the iAccess Client Solutions ODBC driver must be installed on your system.

```
pip install git+https://bitbucket.org/jacksonwebservices/iseries_util
```

> Note on Linux you will need to have unixodbc and unixodbc-dev packages installed from your distribution, as well as the iaccess drivers from IBM..

## CLI Commands

### copy_objects

This is used to copy objects from one iSeries system to another. It relies on the two systems being able to communicate with each other via FTP. Credentials must be available for both systems.

First create your connection files:

```cmd
> copy_objects create-dsn-files
Source system IP or DNS: 192.168.1.2
Source system username: username
Source system password:
Destination system IP or DNS: 192.168.1.3
Destination system username: username
Destination system password:
2020-05-28 13:54:15,428:INFO:copy_objects:writing to source.dsn
2020-05-28 13:54:15,430:INFO:copy_objects:writing to dest.dsn
> ls *.dsn
dest.dsn  source.dsn
```

Now copy objects and/or entire schemas (use `--whatif` like this to test without making changes):
```cmd
> copy_objects --obj=schema.long_table_name --obj=schema.long_view_name --schema=schemaname --whatif
```
